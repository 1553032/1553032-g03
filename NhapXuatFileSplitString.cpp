#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
using namespace std;

struct monomial
{
	int sobien = 1;
	float coe;
	char *var;
	unsigned short *exp;
	monomial *pNext;
	monomial()
	{}
	monomial(monomial &a)
	{
		coe = a.coe;
		exp = a.exp;
		var = a.var;
		sobien = a.sobien;
	}
	monomial &operator=(monomial a)
	{
		monomial tmp(a);
		tmp.pNext = a.pNext;
		return tmp;
	}
	monomial operator+(monomial a)
	{
		monomial tmp;
		tmp.coe = coe + a.coe;
		tmp.exp = a.exp;
		tmp.var = a.var;
		return tmp;
	}
	monomial operator-(monomial a)
	{
		monomial tmp;
		tmp.coe = coe - a.coe;
		tmp.exp = a.exp;
		tmp.var = a.var;
		return tmp;
	}
	monomial operator*(monomial a)
	{
		monomial tmp = a; // luu a 
		tmp.coe *= coe;
		int m, n;
		m = strlen(a.var);
		n = strlen(var);
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
				if (a.var[i] == var[j]) // gap bien giong nhau 
				{
					tmp.exp[i] += exp[j]; // cap nhat mu 
				}
		}
		int count = -1;
		for (int i = 0; i < n; i++)
		{
			bool test = false;
			for (int j = 0; j < m; j++)
				if (var[i] == a.var[j])
					test = true; // bien cua b da ton tai trong don thuc moi 
			if (!test) // khong co bien cua b trong don thuc moi  
			{
				count++;
				tmp.var[m + count] = var[i];
				tmp.exp[m + count] = exp[i];
			}
		}
		return tmp;
	}
};

class Polymial
{
public:
	string loadfile(string infile);//take data from file to execute
	void storeFile(string outfile, int &f_count);//input data to file	
	void createPoly();//create a polynomial list default	
	void splitPoly(string infile);//split polynomial into monomial	
private:
	monomial *pHead;
	monomial *pTail;
}
string Polymial::loadfile(string infile)
{
	string tmp = "";//using this (string) tmp to store data in file text;
	ifstream f_input;
	f_input.open(infile);
	if (f_input.is_open())
	{
		getline(f_input, tmp);//input polynomial from string into tmp;
	}
	f_input.close();
	return tmp;
}

string convertFloat(float a)
{
	stringstream ss;
	ss << a;
	return ss.str();
}

//input data to file
void Polymial::storeFile(string outfile, int &f_count)
{
	string strtmp = "";
	string value_tmp;
	ofstream f_output;
	f_output.open(outfile, ios::app);
	if (f_output.is_open())
	{
		f_output << "F" << f_count << endl;
		f_count++;
		f_output << "---";
		monomial *tmp = pHead;
		while (tmp != NULL)
		{
			value_tmp = convertFloat(tmp->coe);
			strtmp += value_tmp;
			value_tmp = convertshort(*tmp->exp);
			char *var_tmp = tmp->var;
			char var_tmp2 = *var_tmp;
			strtmp += var_tmp2;
			strtmp += "^" + value_tmp;
			if (tmp->pNext != NULL)
			{
				if (tmp->pNext->coe > 0)
				{
					strtmp += "+";
				}
			}
			tmp = tmp->pNext;
		}
	}
	cout << strtmp << endl;
	f_output << strtmp << endl;
	f_output.close();
}

//split polynomial into monomial
void Polymial::splitPoly(string infile)
{
	string poly = loadfile(infile);
	monomial tmp;
	string store;

	int length = poly.length();
	for (int i = 0; i < length; i++)
	{
		store = "";
		if (poly[i] == '-')
		{
			store += poly[i];
			i++;
		}
		if (i > 0)
		{
			if (!checkOperate(poly[i]))
			{
				if (poly[i - 1] == '-')
				{
					store += '-';
				}
			}
		}
		while (!checkOperate(poly[i]))
		{
			store += poly[i];
			i++;
		}
		tmp.coe = atof(store.c_str());

		int count = 0;
		int j = i + 1;


		while (poly[j] != '+' && poly[j] != '-') // dem so bien 
		{
			if (poly[j] >= 'a' && poly[j] <= 'z')
			{
				count++;
			}
			j++;
		}

		tmp.sobien = count;
		tmp.var = new char[count];
		tmp.exp = new unsigned short[count];
		count = -1;

		while (poly[i] != '+' && poly[i] != '-')
		{
			if (poly[i] >= 'a' && poly[i] <= 'z') // gap bien 
			{
				++count;
				tmp.var[count] = poly[i]; // lay bien 

				string exp_store = "";

				if (poly[i + 1] == '^') // kiem tra mu 
				{
					i += 2;

					while (poly[i] != '*' && poly[i] != '+' && poly[i] != '-') // lay so mu 
					{
						exp_store += poly[i];
						i++;
					}
				}
				if (exp_store == "")
				{
					tmp.exp[count] = 1;
					i++;
				}
				else
				{
					char *exp_char = new char[exp_store.length() + 1];
					strcpy(exp_char, exp_store.c_str());
					tmp.exp[count] = atoi(exp_char);
				}
				continue;
			}
			i++;
		}
		enqueue(tmp);
		//cout << pHead->pNext->pNext->coe << endl;
	}
}